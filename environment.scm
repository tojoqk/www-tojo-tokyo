;;; TojoQK's website
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of TojoQK's website.
;;;
;;; TojoQK's website is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; TojoQK's website is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with TojoQK's website.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix build-system guile)
             (gnu packages)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages python-web)
             (gnu packages haskell-xyz)
             (gnu packages password-utils))

(package
  (name "www-tojo-tokyo")
  (version "0.0.0")
  (source (string-append (getcwd) "/www-tojo-tokyo-" version ".tar.gz"))
  (build-system guile-build-system)
  (inputs
   `(("guile" ,guile-3.0)
     ("haunt" ,guile3.0-haunt)
     ("awscli" ,awscli)
     ("make" ,gnu-make)
     ("giule-commonmark" ,guile3.0-commonmark)
     ("ghc-pandoc" ,ghc-pandoc)
     ("password-store" ,password-store)))
  (synopsis "TojoQK's web site")
  (description "This project is TojoQK's web site.")
  (home-page "http://www.tojo.tokyo")
  (license license:gpl3+))
