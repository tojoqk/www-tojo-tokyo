.PHONEY: build clean deploy serve

build:
	haunt build

clean:
	rm -rf site

serve: build
	haunt serve --watch

deploy: build
	rsync -av --delete site/ web:/srv/www/
