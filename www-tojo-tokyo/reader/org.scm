;;; TojoQK's website --- TojoQK's personal website
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of TojoQK's website
;;;
;;; TojoQK's website is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; TojoQK's website is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with TojoQK's website.  If not, see <https://www.gnu.org/licenses/>.

(define-module (www-tojo-tokyo reader org)
  #:use-module (haunt post)
  #:use-module (haunt reader)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (sxml simple)
  #:use-module (sxml xpath)
  #:export (org-reader))

(define open-process (@@ (ice-9 popen) open-process))

(define (org-string->html-string str)
  (receive (read-port write-port pid)
      (open-process OPEN_BOTH "pandoc" "-f" "org")
    (put-string write-port str)
    (close-port write-port)
    (let ((result (get-string-all read-port)))
      (close-port read-port)
      (let ((status (cdr (waitpid pid))))
        (unless (zero? status)
          (error "pandoc return a non-zero status-code:" status)))
      result)))

(define org-reader
  (make-reader (make-file-extension-matcher "org")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (let* ((metadata (read-metadata-headers port))
                            (html-string (org-string->html-string
                                          (get-string-all port))))
                       (values metadata
                               ((sxpath '(dummy *))
                                (xml->sxml
                                 (string-append "<dummy>"
                                                html-string
                                                "</dummy>"))))))))))
