;;; TojoQK's website --- TojoQK's personal website
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This file is part of TojoQK's website
;;;
;;; TojoQK's website is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; TojoQK's website is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with TojoQK's website.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (haunt asset)
             (haunt site)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader)
             (www-tojo-tokyo reader org)
             (srfi srfi-19)
             (haunt post)
             (sxml simple))

(define site-name "TojoQK")

(define (date->string/tojoqk date)
  (date->string date "~Y年~m月~d日 ~H時~M分"))

(define (post-slug/tojoqk post)
  (post-ref post 'id))

(define (updated->date-or-false updated)
  (cond
   ((date? updated) updated)
   ((string? updated) (string->date* updated))
   (else updated)))

(define* (post-url post #:optional prefix)
  (string-append "/"
                 (if prefix
                     (string-append prefix "/")
                     "")
                 (post-slug/tojoqk post)
                 ".html"))

(define (theme/tojoqk)
  (theme
   #:layout
   (lambda (site title obj)
     (let ((og (car obj))
           (sxml (cdr obj)))
       `((doctype "html")
         (head
          (@ (lang "ja")
             (prefix "og: http://ogp.me/ns#"))
          (meta (@ (charset "utf-8")))
          (link (@ (rel "stylesheet") (href "/static/css/blog.css")))
          ,@(map
             (lambda (pxp)
               `(link (@ (rel "icon")
                         (type "image/png")
                         (href ,(string-append "/static/image/QK-" pxp ".png"))
                         (sizes pxp))))
             (list "16x16" "32x32" "96x96"))
          ,@(map
             (lambda (pxp)
               `(link (@ (rel "apple-touch-icon")
                         (href ,(string-append "/static/image/QK-" pxp ".png"))
                         (sizes ,pxp))))
             (list "57x57" "60x60" "72x72" "76x76" "114x114" "152x152" "180x180"))
          (meta (@ (property "og:title") (content ,(assoc-ref og 'title))))
          (meta (@ (property "og:type") (content ,(assoc-ref og 'type))))
          (meta (@ (property "og:url") (content ,(assoc-ref og 'url))))
          ,@(cond
             ((assoc-ref og 'description)
              => (lambda (description)
                   `((meta (@ (name "description") (content ,description)))
                     (meta (@ (property "og:description") (content ,description))))))
             (else '()))
          (meta (@ (property "og:site_name") (content ,site-name)))
          (meta (@ (property "og:image")
                   (content
                    ,(string-append "https://"
                                    (site-domain site)
                                    (or (assoc-ref og 'image) "/static/image/QK.png")))))
          (meta (@ (name "viewport")
                   (content "initial-scale=1.0")
                   (width "device-width")))
          (title
           ,(if (string=? title site-name)
                site-name
                (string-append title " -- " site-name))))
         (body
          (div (@ (class "world"))
               (nav (@ (class "nav-bar"))
                    (ul (@ (class "nav-list"))
                        (li (@ (class "nav-logo"))
                            (a (@ (href "/")) "TojoQK"))
                        (li (@ (class "nav-item"))
                            (a (@ (href "/about-me.html")) "About me"))
                        (li (@ (class "nav-item"))
                            (a (@ (href "/contact.html")) "Contact"))))
               (article (@ (class "main"))
                        ,sxml))))))
   #:collection-template
   (lambda (site title posts prefix)
     (cons
      `((title . ,title)
        (type . "website")
        (description . "TojoQKの日記")
        (url . "/"))
      
      `((p (a (@ (rel "me")
                 (href "https://mastodon.tojo.tokyo/@tojoqk"))
              "Mastodon(@tojoqk@mastodon.tojo.tokyo)")
           " でも活動しています")
        (h1 "最近の投稿")
        (ul (@ (class "post-list"))
            ,@(map (lambda (post)
                     (let ((url (post-url post)))
                       `(li (div (@ (class "post-list-item"))
                                 (a (@ (href ,url))
                                    ,(post-ref post 'title))
                                 (div (@ (class "post-list-date"))
                                      ,(date->string/tojoqk
                                        (post-date post)))))))
                   posts)))))
   #:post-template
   (lambda (post)
     (cons
      `((title . ,(post-ref post 'title))
        (type . "article")
        (url . ,(post-url post))
        (description . ,(post-ref post 'description)))
      `((div (h1 (@ (class "post-title"))
                 (a (@ (href "")) ,(post-ref post 'title)))
             (div (@ (class "post-date"))
                  "投稿日: ",(date->string/tojoqk (post-date post)))
             ,@(cond
                ((updated->date-or-false (post-ref post 'updated))
                 => (lambda (updated)
                      `((div (@ (class "post-date"))
                             "更新日: " ,(date->string/tojoqk updated)
                             " ("
                             (a (@ (href ,(string-append
                                           "https://gitlab.com/"
                                           "tojoqk/www-tojo-tokyo/-/commits/master/"
                                           (post-file-name post))))
                                "更新履歴")
                             ")"))))
                (else '())))
        (div ,(post-sxml post))
        (ul (@ (class "post-end"))
            (li (a (@ (href "/")) "投稿の一覧"))
            (li (a (@ (href ,(string-append "https://gitlab.com/tojoqk/www-tojo-tokyo/-/tree/master/"
                                            (post-file-name post))))
                   "このページのソースコード"))
            (li (a (@ (href "feed.xml")) "RSSフィードを購読する"))))))))

(define (blog/tojoqk)
  (blog #:theme (theme/tojoqk)
        #:collections (list (list site-name "index.html" posts/reverse-chronological))))

(site #:title site-name
      #:domain "www.tojo.tokyo"
      #:default-metadata
      '((author . "Masaya Tojo")
        (email  . "masaya@tojo.tokyo"))
      #:readers (list sxml-reader org-reader)
      #:builders (list (blog/tojoqk)
                       (atom-feed)
                       (static-directory "static"))
      #:make-slug post-slug/tojoqk)
